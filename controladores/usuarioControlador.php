<?php

if ($peticionAjax) {
    require_once "../modelos/usuarioModelo.php";
} else {
    require_once "./modelos/usuarioModelo.php";
}

class usuarioControlador extends usuarioModelo
{
    /*--------- CONTROLADOR PARA AGREGAR USUARIO ---------*/
    public function agregar_usuario_controlador()
    {
        $documento = mainModel::limpiar_cadena($_POST['usuario_dni_reg']);
        $nombre = mainModel::limpiar_cadena($_POST['usuario_nombre_reg']);
        $apellido = mainModel::limpiar_cadena($_POST['usuario_apellido_reg']);
        $telefono = mainModel::limpiar_cadena($_POST['usuario_telefono_reg']);
        $celular = mainModel::limpiar_cadena($_POST['usuario_celular_reg']);


        $usuario = mainModel::limpiar_cadena($_POST['usuario_usuario_reg']);
        $email=mainModel::limpiar_cadena($_POST['usuario_email_reg']);
        $clave1 = mainModel::limpiar_cadena($_POST['usuario_clave_1_reg']);
        $clave2 = mainModel::limpiar_cadena($_POST['usuario_clave_2_reg']);


        $privilegio = mainModel::limpiar_cadena($_POST['usuario_privilegio_reg']);

        /*--------- COMPROBAR CAMPOS VACIOS ---------*/
        if ($documento =="" || $nombre == "" || $apellido == "" || $usuario == "" || $clave1 == "" ||
            $clave2 == "") {
            $alerta = [
                "Alerta" => "simple",
                "Titulo" => "Ocurrio un error inesperado",
                "Texto" => "No has llenado todos los campos obligatorios",
                "Tipo" => "error"
            ];
            echo json_encode($alerta);
            exit();
        }

        /*== Verificando integridad de los datos ==*/
			if(mainModel::verificar_datos("[0-9-]{10,20}",$documento)){
				$alerta=[
					"Alerta"=>"simple",
					"Titulo"=>"Ocurrió un error inesperado",
					"Texto"=>"El numero de documento no coincide con el formato solicitado",
					"Tipo"=>"error"
				];
				echo json_encode($alerta);
				exit();
            }
            if(mainModel::verificar_datos("[a-zA-ZáéíóúÁÉÍÓÚñÑ ]{4,35}",$nombre)){
				$alerta=[
					"Alerta"=>"simple",
					"Titulo"=>"Ocurrió un error inesperado",
					"Texto"=>"El NOMBRE no coincide con el formato solicitado",
					"Tipo"=>"error"
				];
				echo json_encode($alerta);
				exit();
            }
            if(mainModel::verificar_datos("[a-zA-ZáéíóúÁÉÍÓÚñÑ ]{4,35}",$apellido)){
				$alerta=[
					"Alerta"=>"simple",
					"Titulo"=>"Ocurrió un error inesperado",
					"Texto"=>"El APELLIDO no coincide con el formato solicitado",
					"Tipo"=>"error"
				];
				echo json_encode($alerta);
				exit();
            }
            if($telefono!=""){
				if(mainModel::verificar_datos("[0-9()+]{6,20}",$telefono)){
					$alerta=[
						"Alerta"=>"simple",
						"Titulo"=>"Ocurrió un error inesperado",
						"Texto"=>"El TELEFONO no coincide con el formato solicitado",
						"Tipo"=>"error"
					];
					echo json_encode($alerta);
					exit();
                }
            }
            if($celular!=""){
                if(mainModel::verificar_datos("[0-9()+]{6,20}",$celular)){
                    $alerta=[
                        "Alerta"=>"simple",
                        "Titulo"=>"Ocurrió un error inesperado",
                        "Texto"=>"El CELULAR no coincide con el formato solicitado",
                        "Tipo"=>"error"
                    ];
                    echo json_encode($alerta);
                    exit();
                }
            }
            if(mainModel::verificar_datos("[a-zA-Z0-9]{1,35}",$usuario)){
                $alerta=[
                    "Alerta"=>"simple",
                    "Titulo"=>"Ocurrió un error inesperado",
                    "Texto"=>"El NOMBRE DE USUARIO no coincide con el formato solicitado",
                    "Tipo"=>"error"
                ];
                echo json_encode($alerta);
                exit();
            }
            if(mainModel::verificar_datos("[a-zA-Z0-9$@.-]{7,100}",$clave1) || mainModel::verificar_datos("[a-zA-Z0-9$@.-]{7,100}",$clave2)){
                $alerta=[
                    "Alerta"=>"simple",
                    "Titulo"=>"Ocurrió un error inesperado",
                    "Texto"=>"Las Contraseñas no coinciden con el formato solicitado",
                    "Tipo"=>"error"
                ];
                echo json_encode($alerta);
                exit();
            }

              /*== comprobando el Documento que no se repita en la base de datos==*/
              $check_dni=mainModel::ejecutar_consulta_simple("SELECT Usu_Documento FROM tbl_usuario WHERE Usu_Documento='$documento'");
            if($check_dni->rowCount()>0){
                $alerta=[
                    "Alerta"=>"simple",
                    "Titulo"=>"Ocurrió un error inesperado",
                    "Texto"=>"El DNI ingresado ya se encuentra registrado en el sistema",
                    "Tipo"=>"error"
                ];
                echo json_encode($alerta);
                exit();
            }
             /*== comprobando el Usuario que no se repita en la base de datos ==*/
            $check_user=mainModel::ejecutar_consulta_simple("SELECT Usu_Usuario FROM tbl_usuario WHERE Usu_Usuario='$usuario'");
            if($check_user->rowCount()>0){
                $alerta=[
                    "Alerta"=>"simple",
                    "Titulo"=>"Ocurrió un error inesperado",
                    "Texto"=>"El USUARIO ingresado ya se encuentra registrado en el sistema",
                    "Tipo"=>"error"
                ];
                echo json_encode($alerta);
                exit();
            }
           /*== comprobando Email ==*/
           if($email!=""){
                if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $check_email=mainModel::ejecutar_consulta_simple("SELECT Usu_Email FROM tbl_usuario WHERE Usu_Email='$email'");
                        if($check_email->rowCount()>0){
                            $alerta=[
                                "Alerta"=>"simple",
                                "Titulo"=>"Ocurrió un error inesperado",
                                "Texto"=>"El E-MAIL ingresado ya se encuentra registrado en el sistema",
                                "Tipo"=>"error"
                            ];
                            echo json_encode($alerta);
                            exit();
                        }
                }else{
                    $alerta=[
                    "Alerta"=>"simple",
                    "Titulo"=>"Ocurrió un error inesperado",
                    "Texto"=>"El E-MAIL ingresado no tiene el formato correcto",
                    "Tipo"=>"error"
                    ];
                    echo json_encode($alerta);
                    exit();
                }
            }
            /*== comprobando claves ==*/
              if ($clave1!=$clave2) {
                $alerta=[
                    "Alerta"=>"simple",
                    "Titulo"=>"Ocurrió un error inesperado",
                    "Texto"=>"Las contraseñas no coinciden",
                    "Tipo"=>"error"
                    ];
                    echo json_encode($alerta);
                    exit();
              }else {
                  $clave=mainModel::encryption($clave1);
                }
                 /*== comprobando privilegio ==*/

                 if ($privilegio<1 || $privilegio>3) {
                    $alerta=[
                        "Alerta"=>"simple",
                        "Titulo"=>"Ocurrió un error inesperado",
                        "Texto"=>"El privilegio selecionado no es valido",
                        "Tipo"=>"error"
                        ];
                        echo json_encode($alerta);
                        exit();
                 }

                 $datos_usuario_reg=[
                    "DNI"=>$documento,
                    "Nombre"=>$nombre,
                    "Apellido"=>$apellido,
                    "Telefono"=>$telefono,
                    "Celular"=>$celular,
                    "Email"=>$email,
                    "Usuario"=>$usuario,
                    "Clave"=>$clave,
                    "Estado"=>"Activa",
                    "Privilegio"=>$privilegio
                  ];
                 $agregar_usuario=usuarioModelo::agregar_usuario_modelo($datos_usuario_reg);

            if ($agregar_usuario->rowCount() == 1) {
                $alerta = [
                    "Alerta" => "limpiar",
                    "Titulo" => "Usuario Registrado",
                    "Texto" => "Los datos del usuario han sido registrado con exito.",
                    "Tipo" => "success",
                ];
            } else {
                $alerta = [
                    "Alerta" => "simple",
                    "Titulo" => "Ocurrió un error inesperado",
                    "Texto" => "No hemos podido registrar el usuario.",
                    "Tipo" => "error",
                ];
            }
            
            echo json_encode($alerta);

                
    }/*fin del controlador */

}
