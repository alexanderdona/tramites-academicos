<?php
    
    require_once "mainModel.php";

    class usuarioModelo extends mainModel{

        /*--------- Modelo agregar usuario ---------*/
        protected static function agregar_usuario_modelo($datos){
            $sql=mainModel::conectar()->prepare("INSERT INTO tbl_usuario(Usu_Documento,Usu_Nombres,Usu_Apellidos,Usu_Telefono,Usu_Celular,Usu_Email,Usu_Usuario,Usu_Clave,Usu_Estado,Usu_Privilegio) VALUES(:DNI,:Nombre,:Apellido,:Telefono,:Celular,:Email,:Usuario,:Clave,:Estado,:Privilegio)");

            $sql->bindParam(":DNI",$datos['DNI']);
            $sql->bindParam(":Nombre",$datos['Nombre']);
            $sql->bindParam(":Apellido",$datos['Apellido']);
            $sql->bindParam(":Telefono",$datos['Telefono']);
            $sql->bindParam(":Celular",$datos['Celular']);
            $sql->bindParam(":Email",$datos['Email']);
            $sql->bindParam(":Usuario",$datos['Usuario']);
            $sql->bindParam(":Clave",$datos['Clave']);
            $sql->bindParam(":Estado",$datos['Estado']);
            $sql->bindParam(":Privilegio",$datos['Privilegio']);
            $sql->execute();

            return $sql;
        }

    }