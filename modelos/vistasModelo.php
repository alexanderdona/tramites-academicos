<?php

class vistasModelo
{

    /*--------- Modelo obtener vistas ---------*/
    protected static function obtener_vistas_modelo($vistas)
    {
        $listaBlanca = [
            "ficha-list", "traslado-new", "ficha-search", "client-update", "reporte", "reingreso-new", "home",
            "aplazamiento-new", "retiro-new", "item-new", "item-list", "item-search", "item-update", "reservation-list", "reservation-new",
            "certificacion-new", "reservation-search", "reservation-update", "user-list",
            "revision-new", "user-new", "user-search", "user-update", "ficha-new"
        ];
        if (in_array($vistas, $listaBlanca)) {
            if (is_file("./vistas/contenidos/" . $vistas . "-view.php")) {
                $contenido = "./vistas/contenidos/" . $vistas . "-view.php";
            } else {
                $contenido = "404";
            }
        } elseif ($vistas == "login" || $vistas == "index") {
            $contenido = "login";
        } else {
            $contenido = "404";
        }
        return $contenido;
    }
}
