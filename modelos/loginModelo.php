<?php
	
	require_once "mainModel.php";

	class loginModelo extends mainModel{

		/*--------- Modelo iniciar sesión ---------*/
		protected static function iniciar_sesion_modelo($datos){
			$sql=mainModel::conectar()->prepare("SELECT * FROM tbl_usuario WHERE Usu_Usuario=:Usuario AND Usu_Clave=:Clave AND Usu_Estado='1'");

			$sql->bindParam(":Usuario",$datos['Usuario']);
			$sql->bindParam(":Clave",$datos['Clave']);
			$sql->execute();

			return $sql;
		}

	}