CREATE DATABASE  IF NOT EXISTS `tramites` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `tramites`;
-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: tramites
-- ------------------------------------------------------
-- Server version	5.6.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_aplazamiento`
--

DROP TABLE IF EXISTS `tbl_aplazamiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_aplazamiento` (
  `Apl_Id` int(11) NOT NULL AUTO_INCREMENT,
  `Apl_Numero_Orden` varchar(45) DEFAULT NULL,
  `Apl_Fecha_Orden` date DEFAULT NULL,
  `Apl_RA1` tinyint(1) DEFAULT NULL,
  `Apl_RA2` tinyint(1) DEFAULT NULL,
  `Apl_RA3` tinyint(1) DEFAULT NULL,
  `Apl_RA4` tinyint(1) DEFAULT NULL,
  `Apl_RA5` tinyint(1) DEFAULT NULL,
  `Apl_RA6` tinyint(1) DEFAULT NULL,
  `Apl_Nradicado` varchar(45) DEFAULT NULL,
  `Apl_Estadot` tinyint(1) DEFAULT NULL,
  `Apl_Estado` tinyint(1) DEFAULT NULL,
  `Tbl_Persona_Per_Id` int(11) NOT NULL,
  `Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id` int(11) NOT NULL,
  `Tbl_Usuario_Usu_Id` int(11) NOT NULL,
  `Tbl_Usuario_Tbl_tipo_usuario_TipUsu_Id` int(11) NOT NULL,
  `Tbl_Ficha_Fic_Id` int(11) NOT NULL,
  PRIMARY KEY (`Apl_Id`,`Tbl_Persona_Per_Id`,`Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id`,`Tbl_Usuario_Usu_Id`,`Tbl_Usuario_Tbl_tipo_usuario_TipUsu_Id`,`Tbl_Ficha_Fic_Id`),
  KEY `fk_Tbl_Aplazamiento_Tbl_Persona1_idx` (`Tbl_Persona_Per_Id`,`Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id`),
  KEY `fk_Tbl_Aplazamiento_Tbl_Usuario1_idx` (`Tbl_Usuario_Usu_Id`,`Tbl_Usuario_Tbl_tipo_usuario_TipUsu_Id`),
  KEY `fk_Tbl_Aplazamiento_Tbl_Ficha1_idx` (`Tbl_Ficha_Fic_Id`),
  CONSTRAINT `fk_Tbl_Aplazamiento_Tbl_Ficha1` FOREIGN KEY (`Tbl_Ficha_Fic_Id`) REFERENCES `tbl_ficha` (`Fic_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Aplazamiento_Tbl_Persona1` FOREIGN KEY (`Tbl_Persona_Per_Id`, `Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id`) REFERENCES `tbl_persona` (`Per_Id`, `Tbl_Tipo_Documento_TipDoc_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Aplazamiento_Tbl_Usuario1` FOREIGN KEY (`Tbl_Usuario_Usu_Id`, `Tbl_Usuario_Tbl_tipo_usuario_TipUsu_Id`) REFERENCES `tbl_usuario` (`Usu_Id`, `Tbl_tipo_usuario_TipUsu_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_aplazamiento`
--

LOCK TABLES `tbl_aplazamiento` WRITE;
/*!40000 ALTER TABLE `tbl_aplazamiento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_aplazamiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_certificacion`
--

DROP TABLE IF EXISTS `tbl_certificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_certificacion` (
  `Cer_Id` int(11) NOT NULL AUTO_INCREMENT,
  `Cer_Numero_Orden` varchar(45) DEFAULT NULL,
  `Cer_Fecha_Orden` date DEFAULT NULL,
  `Cer_Empresa` varchar(200) DEFAULT NULL,
  `Cer_RC1` tinyint(1) DEFAULT NULL,
  `Cer_RC2` tinyint(1) DEFAULT NULL,
  `Cer_RC3` tinyint(1) DEFAULT NULL,
  `Cer_RC4` tinyint(1) DEFAULT NULL,
  `Cer_RC5` tinyint(1) DEFAULT NULL,
  `Cer_RC6` tinyint(1) DEFAULT NULL,
  `Cer_RC7` tinyint(1) DEFAULT NULL,
  `Cer_RC8` tinyint(1) DEFAULT NULL,
  `Cer_RC9` tinyint(1) DEFAULT NULL,
  `Cer_Estadot` tinyint(1) DEFAULT NULL,
  `Cer_Estado` tinyint(1) DEFAULT NULL,
  `Tbl_Persona_Per_Id` int(11) NOT NULL,
  `Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id` int(11) NOT NULL,
  `Tbl_Persona_Tbl_Municipio_Mun_Id` int(11) NOT NULL,
  `Tbl_Usuario_Usu_Id` int(11) NOT NULL,
  `Tbl_Usuario_Tbl_tipo_usuario_TipUsu_Id` int(11) NOT NULL,
  `Tbl_Ficha_Fic_Id` int(11) NOT NULL,
  PRIMARY KEY (`Cer_Id`,`Tbl_Persona_Per_Id`,`Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id`,`Tbl_Persona_Tbl_Municipio_Mun_Id`,`Tbl_Usuario_Usu_Id`,`Tbl_Usuario_Tbl_tipo_usuario_TipUsu_Id`,`Tbl_Ficha_Fic_Id`),
  KEY `fk_Tbl_Certificacion_Tbl_Persona1_idx` (`Tbl_Persona_Per_Id`,`Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id`,`Tbl_Persona_Tbl_Municipio_Mun_Id`),
  KEY `fk_Tbl_Certificacion_Tbl_Usuario1_idx` (`Tbl_Usuario_Usu_Id`,`Tbl_Usuario_Tbl_tipo_usuario_TipUsu_Id`),
  KEY `fk_Tbl_Certificacion_Tbl_Ficha1_idx` (`Tbl_Ficha_Fic_Id`),
  CONSTRAINT `fk_Tbl_Certificacion_Tbl_Ficha1` FOREIGN KEY (`Tbl_Ficha_Fic_Id`) REFERENCES `tbl_ficha` (`Fic_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Certificacion_Tbl_Persona1` FOREIGN KEY (`Tbl_Persona_Per_Id`, `Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id`, `Tbl_Persona_Tbl_Municipio_Mun_Id`) REFERENCES `tbl_persona` (`Per_Id`, `Tbl_Tipo_Documento_TipDoc_Id`, `Tbl_Municipio_Mun_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Certificacion_Tbl_Usuario1` FOREIGN KEY (`Tbl_Usuario_Usu_Id`, `Tbl_Usuario_Tbl_tipo_usuario_TipUsu_Id`) REFERENCES `tbl_usuario` (`Usu_Id`, `Tbl_tipo_usuario_TipUsu_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_certificacion`
--

LOCK TABLES `tbl_certificacion` WRITE;
/*!40000 ALTER TABLE `tbl_certificacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_certificacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_departamento`
--

DROP TABLE IF EXISTS `tbl_departamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_departamento` (
  `Dep_Id` int(11) NOT NULL AUTO_INCREMENT,
  `Dep_Descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Dep_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_departamento`
--

LOCK TABLES `tbl_departamento` WRITE;
/*!40000 ALTER TABLE `tbl_departamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_departamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_ficha`
--

DROP TABLE IF EXISTS `tbl_ficha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ficha` (
  `Fic_Id` int(11) NOT NULL AUTO_INCREMENT,
  `Fic_Ficha` int(11) DEFAULT NULL,
  `Fic_Especialidad` varchar(200) DEFAULT NULL,
  `Fic_Grupo` varchar(45) DEFAULT NULL,
  `Fic_Estado` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Fic_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_ficha`
--

LOCK TABLES `tbl_ficha` WRITE;
/*!40000 ALTER TABLE `tbl_ficha` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_ficha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_municipio`
--

DROP TABLE IF EXISTS `tbl_municipio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_municipio` (
  `Mun_Id` int(11) NOT NULL AUTO_INCREMENT,
  `Mun_Descripcion` varchar(45) DEFAULT NULL,
  `Tbl_Departamento_Dep_Id` int(11) NOT NULL,
  PRIMARY KEY (`Mun_Id`,`Tbl_Departamento_Dep_Id`),
  KEY `fk_Tbl_Municipio_Tbl_Departamento1_idx` (`Tbl_Departamento_Dep_Id`),
  CONSTRAINT `fk_Tbl_Municipio_Tbl_Departamento1` FOREIGN KEY (`Tbl_Departamento_Dep_Id`) REFERENCES `tbl_departamento` (`Dep_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1103 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_municipio`
--

LOCK TABLES `tbl_municipio` WRITE;
/*!40000 ALTER TABLE `tbl_municipio` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_municipio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_persona`
--

DROP TABLE IF EXISTS `tbl_persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_persona` (
  `Per_Id` int(11) NOT NULL AUTO_INCREMENT,
  `Per_Documento` varchar(15) DEFAULT NULL,
  `Per_Nombre` varchar(45) DEFAULT NULL,
  `Per_Apellido1` varchar(45) DEFAULT NULL,
  `Per_Apellido2` varchar(45) DEFAULT NULL,
  `Per_Barrio` varchar(45) DEFAULT NULL,
  `Per_Direccion` varchar(45) DEFAULT NULL,
  `Per_Telefono` varchar(10) DEFAULT NULL,
  `Per_Celular` varchar(13) DEFAULT NULL,
  `Per_Email` varchar(45) DEFAULT NULL,
  `Per_Estado` tinyint(1) DEFAULT NULL,
  `Tbl_Tipo_Documento_TipDoc_Id` int(11) NOT NULL,
  `Tbl_Municipio_Mun_Id` int(11) NOT NULL,
  `Tbl_Ficha_Fic_Id` int(11) NOT NULL,
  `Tbl_Departamento_Dep_Id` int(11) NOT NULL,
  PRIMARY KEY (`Per_Id`,`Tbl_Tipo_Documento_TipDoc_Id`,`Tbl_Municipio_Mun_Id`,`Tbl_Ficha_Fic_Id`,`Tbl_Departamento_Dep_Id`),
  KEY `fk_Tbl_Persona_Tbl_Tipo_Documento1_idx` (`Tbl_Tipo_Documento_TipDoc_Id`),
  KEY `fk_Tbl_Persona_Tbl_Municipio1_idx` (`Tbl_Municipio_Mun_Id`),
  KEY `fk_Tbl_Persona_Tbl_Ficha1_idx` (`Tbl_Ficha_Fic_Id`),
  KEY `fk_Tbl_Persona_Tbl_Departamento1_idx` (`Tbl_Departamento_Dep_Id`),
  CONSTRAINT `fk_Tbl_Persona_Tbl_Departamento1` FOREIGN KEY (`Tbl_Departamento_Dep_Id`) REFERENCES `tbl_departamento` (`Dep_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Persona_Tbl_Ficha1` FOREIGN KEY (`Tbl_Ficha_Fic_Id`) REFERENCES `tbl_ficha` (`Fic_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Persona_Tbl_Municipio1` FOREIGN KEY (`Tbl_Municipio_Mun_Id`) REFERENCES `tbl_municipio` (`Mun_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Persona_Tbl_Tipo_Documento1` FOREIGN KEY (`Tbl_Tipo_Documento_TipDoc_Id`) REFERENCES `tbl_tipo_documento` (`TipDoc_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_persona`
--

LOCK TABLES `tbl_persona` WRITE;
/*!40000 ALTER TABLE `tbl_persona` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_reingreso`
--

DROP TABLE IF EXISTS `tbl_reingreso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_reingreso` (
  `Rei_Id` int(11) NOT NULL AUTO_INCREMENT,
  `Rei_Numero_Orden` varchar(45) DEFAULT NULL,
  `Rei_Fecha_Orden` date DEFAULT NULL,
  `Rei_RR1` tinyint(1) DEFAULT NULL,
  `Rei_RR2` tinyint(1) DEFAULT NULL,
  `Rei_RR3` tinyint(1) DEFAULT NULL,
  `Rei_RR4` tinyint(1) DEFAULT NULL,
  `Rei_RR5` tinyint(1) DEFAULT NULL,
  `Rei_RR6` tinyint(1) DEFAULT NULL,
  `Rei_RR7` tinyint(1) DEFAULT NULL,
  `Rei_NRadicado` varchar(45) DEFAULT NULL,
  `Rei_Estadot` tinyint(1) DEFAULT NULL,
  `Rei_Estado` tinyint(1) DEFAULT NULL,
  `Tbl_Persona_Per_Id` int(11) NOT NULL,
  `Tbl_Ficha_Fic_Id` int(11) NOT NULL,
  `Tbl_Usuario_Usu_Id` int(11) NOT NULL,
  PRIMARY KEY (`Rei_Id`,`Tbl_Persona_Per_Id`,`Tbl_Ficha_Fic_Id`,`Tbl_Usuario_Usu_Id`),
  KEY `fk_Tbl_Reingreso_Tbl_Persona1_idx` (`Tbl_Persona_Per_Id`),
  KEY `fk_Tbl_Reingreso_Tbl_Ficha1_idx` (`Tbl_Ficha_Fic_Id`),
  KEY `fk_Tbl_Reingreso_Tbl_Usuario1_idx` (`Tbl_Usuario_Usu_Id`),
  CONSTRAINT `fk_Tbl_Reingreso_Tbl_Ficha1` FOREIGN KEY (`Tbl_Ficha_Fic_Id`) REFERENCES `tbl_ficha` (`Fic_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Reingreso_Tbl_Persona1` FOREIGN KEY (`Tbl_Persona_Per_Id`) REFERENCES `tbl_persona` (`Per_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Reingreso_Tbl_Usuario1` FOREIGN KEY (`Tbl_Usuario_Usu_Id`) REFERENCES `tbl_usuario` (`Usu_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_reingreso`
--

LOCK TABLES `tbl_reingreso` WRITE;
/*!40000 ALTER TABLE `tbl_reingreso` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_reingreso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_retiro_voluntario`
--

DROP TABLE IF EXISTS `tbl_retiro_voluntario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_retiro_voluntario` (
  `RetVol_Id` int(11) NOT NULL AUTO_INCREMENT,
  `RetVol_Numero_Orden` varchar(45) DEFAULT NULL,
  `RetVol_Fecha_Orden` date DEFAULT NULL,
  `RetVol_RRV1` tinyint(1) DEFAULT NULL,
  `RetVol_RRV2` tinyint(1) DEFAULT NULL,
  `RetVol_RRV3` tinyint(1) DEFAULT NULL,
  `RetVol_RRV4` tinyint(1) DEFAULT NULL,
  `RetVol_RRV5` tinyint(1) DEFAULT NULL,
  `RetVol_Nradicado` varchar(45) DEFAULT NULL,
  `RetVol_EstadoT` tinyint(1) DEFAULT NULL,
  `RetVol_Estado` tinyint(1) DEFAULT NULL,
  `Tbl_Persona_Per_Id` int(11) NOT NULL,
  `Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id` int(11) NOT NULL,
  `Tbl_Persona_Tbl_Municipio_Mun_Id` int(11) NOT NULL,
  `Tbl_Usuario_Usu_Id` int(11) NOT NULL,
  `Tbl_Usuario_Tbl_tipo_usuario_TipUsu_Id` int(11) NOT NULL,
  `Tbl_Ficha_Fic_Id` int(11) NOT NULL,
  PRIMARY KEY (`RetVol_Id`,`Tbl_Persona_Per_Id`,`Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id`,`Tbl_Persona_Tbl_Municipio_Mun_Id`,`Tbl_Usuario_Usu_Id`,`Tbl_Usuario_Tbl_tipo_usuario_TipUsu_Id`,`Tbl_Ficha_Fic_Id`),
  KEY `fk_Tbl_Retiro_voluntario_Tbl_Persona1_idx` (`Tbl_Persona_Per_Id`,`Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id`,`Tbl_Persona_Tbl_Municipio_Mun_Id`),
  KEY `fk_Tbl_Retiro_voluntario_Tbl_Usuario1_idx` (`Tbl_Usuario_Usu_Id`,`Tbl_Usuario_Tbl_tipo_usuario_TipUsu_Id`),
  KEY `fk_Tbl_Retiro_voluntario_Tbl_Ficha1_idx` (`Tbl_Ficha_Fic_Id`),
  CONSTRAINT `fk_Tbl_Retiro_voluntario_Tbl_Ficha1` FOREIGN KEY (`Tbl_Ficha_Fic_Id`) REFERENCES `tbl_ficha` (`Fic_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Retiro_voluntario_Tbl_Persona1` FOREIGN KEY (`Tbl_Persona_Per_Id`, `Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id`, `Tbl_Persona_Tbl_Municipio_Mun_Id`) REFERENCES `tbl_persona` (`Per_Id`, `Tbl_Tipo_Documento_TipDoc_Id`, `Tbl_Municipio_Mun_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Retiro_voluntario_Tbl_Usuario1` FOREIGN KEY (`Tbl_Usuario_Usu_Id`, `Tbl_Usuario_Tbl_tipo_usuario_TipUsu_Id`) REFERENCES `tbl_usuario` (`Usu_Id`, `Tbl_tipo_usuario_TipUsu_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_retiro_voluntario`
--

LOCK TABLES `tbl_retiro_voluntario` WRITE;
/*!40000 ALTER TABLE `tbl_retiro_voluntario` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_retiro_voluntario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_revision_re`
--

DROP TABLE IF EXISTS `tbl_revision_re`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_revision_re` (
  `Rev_Id` int(11) NOT NULL AUTO_INCREMENT,
  `Rev_Numero_Orden` varchar(45) DEFAULT NULL,
  `Rev_Fecha_Orden` date DEFAULT NULL,
  `Rev_RRE1` tinyint(1) DEFAULT NULL,
  `Rev_RRE2` tinyint(1) DEFAULT NULL,
  `Rev_RRE3` tinyint(1) DEFAULT NULL,
  `Rev_RRE4` tinyint(1) DEFAULT NULL,
  `Rev_RRE5` tinyint(1) DEFAULT NULL,
  `Rev_RRE6` tinyint(1) DEFAULT NULL,
  `Rev_RRE7` tinyint(1) DEFAULT NULL,
  `Rev_RRE8` tinyint(1) DEFAULT NULL,
  `Rev_RRE9` tinyint(1) DEFAULT NULL,
  `Rev_RRE10` tinyint(1) DEFAULT NULL,
  `Rev_Nradicado` varchar(45) DEFAULT NULL,
  `Rev_Estadot` tinyint(1) DEFAULT NULL,
  `Rev_Estado` tinyint(1) DEFAULT NULL,
  `Tbl_Ficha_Fic_Id` int(11) NOT NULL,
  `Tbl_Persona_Per_Id` int(11) NOT NULL,
  `Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id` int(11) NOT NULL,
  `Tbl_Persona_Tbl_Municipio_Mun_Id` int(11) NOT NULL,
  `Tbl_Persona_Tbl_Ficha_Fic_Id` int(11) NOT NULL,
  `Tbl_Persona_Tbl_Departamento_Dep_Id` int(11) NOT NULL,
  `Tbl_Usuario_Usu_Id` int(11) NOT NULL,
  `Tbl_Usuario_Tbl_tipo_usuario_TipUsu_Id` int(11) NOT NULL,
  PRIMARY KEY (`Rev_Id`,`Tbl_Ficha_Fic_Id`,`Tbl_Persona_Per_Id`,`Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id`,`Tbl_Persona_Tbl_Municipio_Mun_Id`,`Tbl_Persona_Tbl_Ficha_Fic_Id`,`Tbl_Persona_Tbl_Departamento_Dep_Id`,`Tbl_Usuario_Usu_Id`,`Tbl_Usuario_Tbl_tipo_usuario_TipUsu_Id`),
  KEY `fk_Tbl_Revision_Re_Tbl_Ficha1_idx` (`Tbl_Ficha_Fic_Id`),
  KEY `fk_Tbl_Revision_Re_Tbl_Persona1_idx` (`Tbl_Persona_Per_Id`,`Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id`,`Tbl_Persona_Tbl_Municipio_Mun_Id`,`Tbl_Persona_Tbl_Ficha_Fic_Id`,`Tbl_Persona_Tbl_Departamento_Dep_Id`),
  KEY `fk_Tbl_Revision_Re_Tbl_Usuario1_idx` (`Tbl_Usuario_Usu_Id`,`Tbl_Usuario_Tbl_tipo_usuario_TipUsu_Id`),
  CONSTRAINT `fk_Tbl_Revision_Re_Tbl_Ficha1` FOREIGN KEY (`Tbl_Ficha_Fic_Id`) REFERENCES `tbl_ficha` (`Fic_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Revision_Re_Tbl_Persona1` FOREIGN KEY (`Tbl_Persona_Per_Id`, `Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id`, `Tbl_Persona_Tbl_Municipio_Mun_Id`, `Tbl_Persona_Tbl_Ficha_Fic_Id`, `Tbl_Persona_Tbl_Departamento_Dep_Id`) REFERENCES `tbl_persona` (`Per_Id`, `Tbl_Tipo_Documento_TipDoc_Id`, `Tbl_Municipio_Mun_Id`, `Tbl_Ficha_Fic_Id`, `Tbl_Departamento_Dep_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Revision_Re_Tbl_Usuario1` FOREIGN KEY (`Tbl_Usuario_Usu_Id`, `Tbl_Usuario_Tbl_tipo_usuario_TipUsu_Id`) REFERENCES `tbl_usuario` (`Usu_Id`, `Tbl_tipo_usuario_TipUsu_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_revision_re`
--

LOCK TABLES `tbl_revision_re` WRITE;
/*!40000 ALTER TABLE `tbl_revision_re` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_revision_re` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tipo_documento`
--

DROP TABLE IF EXISTS `tbl_tipo_documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tipo_documento` (
  `TipDoc_Id` int(11) NOT NULL AUTO_INCREMENT,
  `TipDoc_Descripcion` varchar(45) DEFAULT NULL,
  `TipDoc_Abv` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`TipDoc_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tipo_documento`
--

LOCK TABLES `tbl_tipo_documento` WRITE;
/*!40000 ALTER TABLE `tbl_tipo_documento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_tipo_documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tipo_usuario`
--

DROP TABLE IF EXISTS `tbl_tipo_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tipo_usuario` (
  `TipUsu_Id` int(11) NOT NULL AUTO_INCREMENT,
  `TipUsu_Descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`TipUsu_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tipo_usuario`
--

LOCK TABLES `tbl_tipo_usuario` WRITE;
/*!40000 ALTER TABLE `tbl_tipo_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_tipo_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_traslado`
--

DROP TABLE IF EXISTS `tbl_traslado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_traslado` (
  `Tra_Id` int(11) NOT NULL AUTO_INCREMENT,
  `Tra_Numero_Orden` varchar(45) DEFAULT NULL,
  `Tra_Fecha_Orden` date DEFAULT NULL,
  `Tra_RT1` tinyint(1) DEFAULT NULL,
  `Tra_RT2` tinyint(1) DEFAULT NULL,
  `Tra_RT3` tinyint(1) DEFAULT NULL,
  `Tra_RT4` tinyint(1) DEFAULT NULL,
  `Tra_RT5` tinyint(1) DEFAULT NULL,
  `Tra_RT6` tinyint(1) DEFAULT NULL,
  `Tra_RT7` tinyint(1) DEFAULT NULL,
  `Tra_Nradicado` varchar(45) DEFAULT NULL,
  `Tra_Estadot` tinyint(1) DEFAULT NULL,
  `Tra_Estado` tinyint(1) DEFAULT NULL,
  `Tbl_Persona_Per_Id` int(11) NOT NULL,
  `Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id` int(11) NOT NULL,
  `Tbl_Usuario_Usu_Id` int(11) NOT NULL,
  `Tbl_Ficha_Fic_Id` int(11) NOT NULL,
  PRIMARY KEY (`Tra_Id`,`Tbl_Persona_Per_Id`,`Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id`,`Tbl_Usuario_Usu_Id`,`Tbl_Ficha_Fic_Id`),
  KEY `fk_Tbl_Traslado_Tbl_Persona1_idx` (`Tbl_Persona_Per_Id`,`Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id`),
  KEY `fk_Tbl_Traslado_Tbl_Usuario1_idx` (`Tbl_Usuario_Usu_Id`),
  KEY `fk_Tbl_Traslado_Tbl_Ficha1_idx` (`Tbl_Ficha_Fic_Id`),
  CONSTRAINT `fk_Tbl_Traslado_Tbl_Ficha1` FOREIGN KEY (`Tbl_Ficha_Fic_Id`) REFERENCES `tbl_ficha` (`Fic_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Traslado_Tbl_Persona1` FOREIGN KEY (`Tbl_Persona_Per_Id`, `Tbl_Persona_Tbl_Tipo_Documento_TipDoc_Id`) REFERENCES `tbl_persona` (`Per_Id`, `Tbl_Tipo_Documento_TipDoc_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Traslado_Tbl_Usuario1` FOREIGN KEY (`Tbl_Usuario_Usu_Id`) REFERENCES `tbl_usuario` (`Usu_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_traslado`
--

LOCK TABLES `tbl_traslado` WRITE;
/*!40000 ALTER TABLE `tbl_traslado` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_traslado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_usuario`
--

DROP TABLE IF EXISTS `tbl_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usuario` (
  `Usu_Id` int(11) NOT NULL AUTO_INCREMENT,
  `Usu_Documento` varchar(45) DEFAULT NULL,
  `Usu_Nombres` varchar(45) DEFAULT NULL,
  `Usu_Apellidos` varchar(45) DEFAULT NULL,
  `Usu_Telefono` varchar(10) DEFAULT NULL,
  `Usu_Celular` varchar(15) DEFAULT NULL,
  `Usu_Contraseña` varchar(45) DEFAULT NULL,
  `Usu_Estado` tinyint(1) DEFAULT NULL,
  `Tbl_tipo_usuario_TipUsu_Id` int(11) NOT NULL,
  PRIMARY KEY (`Usu_Id`,`Tbl_tipo_usuario_TipUsu_Id`),
  KEY `fk_Tbl_Usuario_Tbl_tipo_usuario1_idx` (`Tbl_tipo_usuario_TipUsu_Id`),
  CONSTRAINT `fk_Tbl_Usuario_Tbl_tipo_usuario1` FOREIGN KEY (`Tbl_tipo_usuario_TipUsu_Id`) REFERENCES `tbl_tipo_usuario` (`TipUsu_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_usuario`
--

LOCK TABLES `tbl_usuario` WRITE;
/*!40000 ALTER TABLE `tbl_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-13 16:27:32
